package edu.ntnu.idatt2001;

/**
 * GP object class
 * @author Jolse
 */
public class GeneralPractitioner extends Doctor {

    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnose) {
        patient.setDignosis(diagnose);
    }
}
