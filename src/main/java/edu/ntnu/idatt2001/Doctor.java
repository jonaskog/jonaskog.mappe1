package edu.ntnu.idatt2001;

/**
 * Abstarct class for Doctor
 * @author Jolse
 */
public abstract class Doctor extends Employee {

    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Abstract methode to set diagnosis
     * @param patient
     * @param string
     */
    public abstract void setDiagnosis(Patient patient ,String string);
}
