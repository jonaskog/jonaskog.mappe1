package edu.ntnu.idatt2001;

import java.io.FileNotFoundException;
import java.lang.module.FindException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Departments of a Hospital
 * @author Jolse
 */
public class Department {

    private String departmentName;
    private ArrayList<Employee>employees = new ArrayList<>();
    private ArrayList<Patient>patients = new ArrayList<>();


    public Department(String departmentName) {
        this.departmentName = departmentName;
    }


    //aksessor methodes
    public ArrayList<Employee> getEmployees() { return employees; }
    public ArrayList<Patient> getPatients() { return patients; }
    public String getDepartmentName() { return departmentName; }

    //add metode
    public void setDepartmentName(String departmentName) { this.departmentName = departmentName; }

    /**
     * add Employee to department
     * @param employee
     * @return true if sucsessfully added
     */
    public boolean addEmployee(Employee employee){
        for(Employee employee1: this.getEmployees()){
            if(employee1 == employee){
                throw new IllegalArgumentException("Illegal to add existing emplyoee");
            }//passer på at ingen allerede eksisterende Ansatte blir lagt til
        }
        employees.add(employee);
        return true;
    }

    /**
     * add Patient to department
     * @param patient
     * @return true if sucsessfully added
     */
    public boolean addPatient(Patient patient){
        for(Patient patient1 : this.getPatients()){
            if(patient == patient1){
                throw new IllegalArgumentException("Pasienten finnes allerede i registeret");
            }//passer på at ingen allerede eksisterende Pasienter blir lagt til
        }
        patients.add(patient);
        return true;
    }

    /**
     * Remove methode for patients or employees
     * @param person
     * @throws FindException
     */
    public void remove(Person person)throws RemoveException{
            if (employees.contains(person)) {
                employees.remove(person);

            } else if (patients.contains(person)) {
                patients.remove(person);
            }
            else {
                throw new RemoveException();
            }
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(getDepartmentName(), that.getDepartmentName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartmentName());
    }

    @Override
    public String toString() {
        return departmentName;
    }
}
