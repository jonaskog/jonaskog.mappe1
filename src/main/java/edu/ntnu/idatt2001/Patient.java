package edu.ntnu.idatt2001;

/**
 * Patient objekt
 * @author Jolse
 */
public class Patient extends Person implements Diagnosable{

    private String diagnosis = " ";

    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);

    }


    @Override//arvet fra Diagnosable
    public void setDignosis(String diagnose) {
        this.diagnosis = diagnose;
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public String toString() {
        return "Patient" +
                "diagnosis='" + diagnosis + '\'';
    }
}
