package edu.ntnu.idatt2001;

/**
 * thorws this class if a Personobject is not a Employee or Patient
 */
public class RemoveException extends Exception{

    public RemoveException(){super("Person is not in the registrer");}
}
