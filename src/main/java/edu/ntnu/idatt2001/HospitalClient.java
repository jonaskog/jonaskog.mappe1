package edu.ntnu.idatt2001;

import java.lang.module.FindException;
import java.sql.SQLOutput;
/**
 * Class med main metode
 * @author Jolse
 */
public class HospitalClient {

    public static void main(String[] args){
        Hospital hospital = new Hospital("");
        System.out.println("Hospital test: \n");

        //Fyller hospital med pasienter

        HospitalTestData.fillRegisterWithTestData(hospital);
        System.out.println("Hospital added");

        //Sletter en ansatt
        Department department = hospital.getDepartments().get(1);
        Employee employee = department.getEmployees().get(2);

        try { department.remove(employee);
            System.out.println(" Person Fjernet");
        }
        catch(RemoveException re){
            System.out.println(re.getMessage());
        }

        // prøver å Slette en pasisent som ikke finnes i registeret
        Patient lea = new Patient("Lea","Grønning","13070056721");
        try { department.remove(lea);
        }catch(RemoveException re){
            System.out.println(re.getMessage());
        }




    }
}

