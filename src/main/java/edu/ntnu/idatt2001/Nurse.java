package edu.ntnu.idatt2001;

/**
 * Nurse objekt class
 * @author Jolse
 */
public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Nurse: ";
    }
}
