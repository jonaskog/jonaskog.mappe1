package edu.ntnu.idatt2001;

/**
 * Obejkt person
 * @author Jolse
 */
public class Employee extends Person {

   public Employee(String firstName, String lastName, String socialSecurityNumber){
       super(firstName,lastName,socialSecurityNumber);
   }

    @Override
    public String toString() {
        return "Employee:";
    }
}
