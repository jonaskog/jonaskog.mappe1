package edu.ntnu.idatt2001;

/**
 * Object for Surgeon
 * @author Jolse
 */
public class Surgeon extends Doctor {

    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnose) {
        patient.setDignosis(diagnose);
    }
}
