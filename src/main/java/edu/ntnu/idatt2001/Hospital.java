package edu.ntnu.idatt2001;

import java.util.ArrayList;

/**
 * Sykehus objekt
 * @author Jolse
 */
public class Hospital {

    private final String hospitalName = "st Olav hospital" ;
    private ArrayList<Department>departments = new ArrayList<>();

    //siden systemet er til St Olavs vill navnet alltid være det samme
    public Hospital(String hospitalName) { hospitalName = hospitalName ; }

    public String getHospitalName() {
        return hospitalName;
    }
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Methode for adding a new department
     * @param department
     * @return true if sucsessfully added
     */
    public boolean addDepartments(Department department){
        for(Department department1: this.getDepartments()){
            if(department == department1){
                throw new IllegalArgumentException("Department allready exist");
            }
        }
        departments.add(department);
        return true;
    }

    @Override
    public String toString() {
        return   hospitalName + '\'' +
                ", Departments: " + departments ;
    }
}
